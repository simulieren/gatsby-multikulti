import React from 'react'
import get from 'lodash/get'
import Helmet from 'react-helmet'
import {getImageSizes} from '../../util/getImage'

import {
  HeroSection,
  HeroSectionTitle,
  HeroSectionSubTitle,
  HeroSectionImageContainer,
} from '../../components/HeroSection'

import {
  IntroSection,
} from '../../components/Type'

import {ProductCard} from '../../components/ProductCard'
import {PreFooter} from '../../components/PreFooter'

class BlogIndex extends React.Component {
  render() {
    const siteTitle = get(this, 'props.data.site.siteMetadata.title')
    // Get the image array out of props.data
    const images = get(this, 'props.data.allImageSharp.edges')

    return (
      <div>
        <Helmet>
          <title>
            {'Produkte — Das finden Sie bei uns — ' + siteTitle}
          </title>
          <meta
            name="description"
            content="Bei uns finden Sie ein breites Sortiment an unterschiedlichen Produkten aus dem russischen und polnischen Raum. Dabei bieten wir Ihnen nicht nur hochwertige Lebensmittel, sondern auch diverse Non-Food-Produkte an. "
          />
        </Helmet>

        <HeroSection>
          <HeroSectionImageContainer sizes={getImageSizes(images, "Russische Pfannekuchen")} alt="Russische Pfannekuchen" />
          <HeroSectionTitle>Unsere Produkte</HeroSectionTitle>
          <HeroSectionSubTitle>
            Das finden Sie bei uns.
          </HeroSectionSubTitle>
        </HeroSection>

        <IntroSection>
          <div className="mw8 center">
            <h3 className="fw9 f2 f1-ns db lh-title mb3 mb4-l mt0 serif black i">
              Eine große Auswahl
            </h3>

            <div className="flex flex-wrap flex-nowrap-ns">
              <span className="serif f5 f4-ns fw9 mr3 mr4-ns pt2 bt w-20-l">Die Produkte</span>
              <p className="black w-50-l">
                Bei uns finden Sie ein breites Sortiment an unterschiedlichen Produkten aus dem russischen und polnischen Raum. Dabei bieten wir Ihnen nicht nur hochwertige Lebensmittel, sondern auch diverse Non-Food-Produkte an. 
              </p>
            </div>
          </div>
        </IntroSection>

        <section className="w-100 pb3 pb5-ns flex justify-center">
          <div className="flex flex-wrap mw9 w-100">
            <ProductCard sizes={getImageSizes(images, "Russische Lebensmittel - Borsch - Suppe")} title="Russischer Borschtsch" />
            <ProductCard sizes={getImageSizes(images, "Russische und polnische gekochte Teigtaschen")} title="Russische Teigtaschen" />
            <ProductCard sizes={getImageSizes(images, "Russiches Gebäck und Teigwaren")} title="Russisches Gebäck" />
          </div>
        </section>

        <section className="w-100 bg-washed-red pv4 pv5-ns">
          <div className="flex flex-wrap mw8 center">
            <div className="w-100 w-50-l ph3 pa4-ns">
              <h3 className="f4 fw7 mt0 pv3 bb bb--black light-red serif">
                Russisch und polnisch
              </h3>

              <p className="f5 lh-copy black">
                Gerade bei den polnischen und russischen Lebensmitteln treffen
                Sie bei uns eine große Auswahl an Produkten an, die Sie
                überzeugen wird. Ob klassische russische Süßigkeiten, die bei
                keiner Feier fehlen dürfen, oder klassische polnische
                Wurstwaren, die durch ihren besonders würzigen und deftigen
                Geschmack begeistern – bei uns bleibt keiner Ihrer Wünsche
                unerfüllt.
              </p>
            </div>

            <div className="w-100 w-50-l ph3 pa4-ns">
              <h3 className="f4 fw7 mt0 pv3 bb bb--black light-red serif">
                Süßes und Gebäck
              </h3>

              <p className="f5 lh-copy black">
                Russische Süßigkeiten, wie Bonbons, Sefir oder anderes, zeichnen
                sich vor allem dadurch aus, dass sie besonders süß sind. Wir
                haben im MultiKulti Markt eine große Auswahl an verschiedenen
                Süßigkeiten für Sie, mit denen Sie sich selbst und Ihre Gäste
                beim nächsten Treffen bei einem Kaffee oder Tee erfreuen können.
              </p>
            </div>

            <div className="w-100 w-50-l ph3 pa4-ns">
              <h3 className="f4 fw7 mt0 pv3 bb bb--black light-red serif">
                Getränke und Tee
              </h3>

              <p className="f5 lh-copy black">
                Apropos Tee: In unserem Sortiment befinden sich auch zahlreiche
                Teesorten, die in Russland und Polen besonders beliebt sind.
                Eine breite Auswahl an Limonaden, Säften und alkoholischen
                Getränken ergänzt unser Sortiment zusätzlich und gibt Ihnen die
                Möglichkeit, Ihr Lieblingsgetränk bei uns zu erwerben.
              </p>
            </div>

            <div className="w-100 w-50-l ph3 pa4-ns">
              <h3 className="f4 fw7 mt0 pv3 bb bb--black light-red serif">
                Fleisch-, Wurst- und Fischwaren
              </h3>

              <p className="f5 lh-copy black">
                Zu unseren besonders beliebten Produkten gehören zweifellos die
                russischen und polnischen Fleisch- und Wurstwaren. Diese sind im
                Vergleich zu den klassischen deutschen Produkten besonders
                würzig und erfreuen sich daher großer Beliebtheit. Außerdem
                überzeugen unsere Fleisch- und Wurstprodukte auch durch ihre
                ausgezeichnete Qualität, die sie zu sicheren und gesunden
                Lebensmitteln macht.
              </p>
            </div>

            <div className="w-100 w-50-l ph3 pa4-ns">
              <h3 className="f4 fw7 mt0 pv3 bb bb--black light-red serif">
                Eingefrorene Waren und Konserven
              </h3>

              <p className="f5 lh-copy black">
                In unserem MultiKulti Markt finden Sie außerdem eine breite
                Auswahl an unterschiedlichen Konserven und eingelegten
                Produkten, die sowohl durch ihren tollen Geschmack als auch
                durch ihre lange Haltbarkeit begeistern. Die Produkte sind nach
                klassischen russischen und polnischen Rezepten hergestellt und
                eignen sich somit hervorragend für die traditionelle Küche. So
                können Sie mit den bei uns erworbenen Lebensmitteln einen
                besonders leckeren Borschtsch, fantastische Pelmeni, Wareniki
                und vieles mehr zubereiten.
              </p>
            </div>

            <div className="w-100 w-50-l ph3 pa4-ns">
              <h3 className="f4 fw7 mt0 pv3 bb bb--black light-red serif">
                Sonstiges und Non-Food-Produkte
              </h3>

              <p className="f5 lh-copy black">
                Auch wenn Sie sich noch nicht mit russischen oder polnischen
                Lebensmitteln auskennen, sollten Sie sich bei uns umschauen,
                denn Sie finden bei uns sicher etwas, was interessant für Sie
                ist und was Sie gerne probieren wollen werden.
                <br />
                <br />
                Außer den Lebensmitteln bieten wir Ihnen auch eine Auswahl an
                Zeitungen und Zeitschriften in kyrillischer Schrift an.
              </p>
            </div>
          </div>
        </section>

        <PreFooter />
      </div>
    )
  }
}

export default BlogIndex

export const pageQuery = graphql`
  query ProduktQuery {
    site {
      siteMetadata {
        title
      }
    }
    allImageSharp {
      edges {
        node {
          id
          sizes (maxWidth: 1920) {
            ...GatsbyImageSharpSizes
          }
        }
      }
    }
  }
`
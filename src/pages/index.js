import React from 'react'
import get from 'lodash/get'
import Helmet from 'react-helmet'

import {getImageSizes} from '../util/getImage'

import {
  HeroSection,
  HeroSectionTitle,
  HeroSectionSubTitle,
  HeroSectionImageContainer,
} from '../components/HeroSection.js'
import {
  Intro,
  IntroSection,
  SecondaryButtonLarge,
  SecondaryButtonSmall,
} from '../components/Type'

import {ProductCard} from '../components/ProductCard'
import {ImageSection} from '../components/ImageSection'
import { PreFooter } from '../components/PreFooter'

import bgPatternOlives from '../images/bg-pattern-olives-white.png'

class BlogIndex extends React.Component {
  render() {
    const siteTitle = get(this, 'props.data.site.siteMetadata.title')
    // const posts = get(this, 'props.data.allMarkdownRemark.edges')
    const images = get(this, 'props.data.allImageSharp.edges')
    
    return (
      <div>
        <Helmet>
          <title>{'Willkommen! — ' + siteTitle}</title>
          <meta
            name="description"
            content="Wenn Sie auf der Suche nach leckeren Köstlichkeiten aus dem
                  osteuropäischen Raum sind, gerne russische oder polnische
                  Zeitungen und Zeitschriften lesen oder einfach nur Mal etwas
                  Neues und Besonderes ausprobieren wollen, dann sind Sie bei
                  uns genau richtig."
          />
        </Helmet>

        <div>
          <HeroSection>
            <HeroSectionImageContainer sizes={getImageSizes(images, "brooke")} alt="" />
            <HeroSectionTitle>MultiKulti Markt</HeroSectionTitle>
            <HeroSectionSubTitle>
              Russische und polnische Lebensmittel
            </HeroSectionSubTitle>
          </HeroSection>

          <IntroSection className="mw9">
            <div className="mw8 center">
              <h3 className="fw9 f2 f1-ns db lh-title mb3 mb4-l mt0 serif black i">
                Herzlich Willkommen in unserem MultiKulti Markt für russische
                und polnische Lebensmittel!
              </h3>

              <div className="flex flex-wrap flex-nowrap-ns">
                <span className="serif f5 f4-ns fw9 mr3 mr4-ns pt2 bt w-20-l">Willkommen</span>
                <p className="black w-50-l">
                  Wenn Sie auf der Suche nach leckeren Köstlichkeiten aus dem
                  osteuropäischen Raum sind, gerne russische oder polnische
                  Zeitungen und Zeitschriften lesen oder einfach nur Mal etwas
                  Neues und Besonderes ausprobieren wollen, dann sind Sie bei
                  uns genau richtig.
                </p>
              </div>
            </div>
          </IntroSection>

          <div
            className="w-100 pv3 pv6-ns mh-100 overflow-hidden bg-washed-red"
            style={{
              backgroundImage: `url(${bgPatternOlives})`,
              backgroundSize: 1000,
            }}
          >

            <div className="flex ph3 mw8 center">
              <h2 className="fw9 f2 f1-ns db lh-title mb3 mb4-l serif black i mt0">
                Entdecken Sie unsere Produkte.
              </h2>
            </div>

            <div className="flex ph3 mw8 center lh-copy">
              <span className="serif f5 f4-ns fw9 mr3 mr4-ns pt2 bt w-20-l light-red">
                Produkte
              </span>

              <p className="black f5 f4-ns measure mb4 mb5-l">
                Viele der traditionellen Lebensmittel aus dem russischen und
                polnischen Raum sind etwas anders als die klassischen deutschen
                Lebensmittel. Daher braucht man für viele polnische oder
                russische Rezepte spezielle Produkte, die in normalen
                Supermärkten selten zu finden sind. Auf diese Produkte haben wir
                uns spezialisiert und freuen uns stets, unsere Kunden mit
                hochwertiger und authentischer Ware zu versorgen.
              </p>
            </div>

            <div className="flex flex-wrap mw9 center">
              <ProductCard sizes={getImageSizes(images, "Salzgurken")} title="Wurstwaren" />
              <ProductCard sizes={getImageSizes(images, "Fisch")} title="Fischwaren" />
              <ProductCard sizes={getImageSizes(images, "Chocolates")} title="Süßigkeiten" />
              <ProductCard sizes={getImageSizes(images, "Spices")} title="Gewürze" />
              <ProductCard sizes={getImageSizes(images, "Konserven")} title="Konserven" />
              <ProductCard sizes={getImageSizes(images, "Getränke")} title="Alkohol, Getränke und Non-Food-Produkte" />
            </div>

            <div className="flex w-100 mb3 ph3 mt5-ns justify-center">
              <SecondaryButtonLarge to="/produkte/" className="light-red z-1">
                Erfahren Sie mehr über unsere Produkte
              </SecondaryButtonLarge>
            </div>
          </div>

          <ImageSection sizes={getImageSizes(images, "andy-chilton")}>
            <h3 className="f4 fw7 mt0 pv3 bb bb--black light-red serif">
              Was uns wichtig ist
            </h3>
            <p className="lh-copy mt3 f6">
              <span className="fw9 f2 db lh-title mb3 mb4-l serif black i">
                Klassisch, traditionell und das besondere Heimatgefühl
              </span>
              <span className="f5 db-l black">
                Mit unseren Produkten können Sie klassische russische oder
                polnische Gerichte zubereiten und damit Ihre Familie, Ihre
                Gäste und natürlich auch Sie selbst erfreuen. Auch wenn
                Sie keine Verbindung zu der russischen oder der polnischen
                Kultur haben, finden Sie bei uns sicherlich interessante
                Lebensmittel und Produkte, die Ihnen gefallen werden.
                Probieren Sie beispielsweise die beliebten polnischen
                Süßigkeiten, köstliche russische Fleischprodukte oder
                eines der vielen traditionellen Gerichte, die ganz genauso
                wie in Russland oder Polen schmecken und deren besonderer
                Geschmack jedes Herz im Sturm erobert.
              </span>

              <div>
                <SecondaryButtonSmall
                  to="/produkte/"
                  className="light-red z-1 mt3"
                >
                  Erfahren Sie mehr über unsere Spezialitäten
                </SecondaryButtonSmall>
              </div>
            </p>
          </ ImageSection>

          <Intro>
            <h3 className="fw9 f2 db lh-title mt4 mb3 mb4-l serif black i">
              Aus dem Emsland
            </h3>
            Wenn Sie aus der Umgebung der schönen Dörfer Herzlake und Haselünne
            kommen, dann kommen Sie doch vorbei und probieren Sie bei uns
            Besonderheiten aus den polnischen und russischen Küchen aus. Auch
            unter unseren anderen Produkten finden Sie sicherlich etwas
            Interessantes, das Sie gerne genauer anschauen möchten.

            <div className="flex w-100 mt3 mb4 mt5-ns justify-center">
              <SecondaryButtonLarge to="/ueber-uns/" className="light-red z-1">
                Erfahren Sie mehr über uns
              </SecondaryButtonLarge>
            </div>
          </Intro>

          <PreFooter />
        </div>
      </div>
    )
  }
}

export default BlogIndex

export const pageQuery = graphql`
  query IndexQuery {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      edges {
        node {
          excerpt
          fields {
            slug
          }
          frontmatter {
            date(formatString: "DD MMMM, YYYY")
            title
          }
        }
      }
    }
    allImageSharp {
      edges {
        node {
          id
          sizes (maxWidth: 1920) {
            ...GatsbyImageSharpSizes
          }
        }
      }
    }
  }
`

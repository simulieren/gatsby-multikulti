import React from 'react'
import get from 'lodash/get'
import Helmet from 'react-helmet'
import { getImageSizes } from '../../util/getImage'

import {
  HeroSection,
  HeroSectionTitle,
  HeroSectionSubTitle,
  HeroSectionImageContainer,
} from '../../components/HeroSection'
import { Intro, IntroSection } from '../../components/Type'

import { ImageSection } from '../../components/ImageSection'
import { PreFooter } from '../../components/PreFooter'

import bgPattern from './../../images/3.svg'

class BlogIndex extends React.Component {
  render() {
    const siteTitle = get(this, 'props.data.site.siteMetadata.title')
    // Get the image array out of props.data
    const images = get(this, 'props.data.allImageSharp.edges')

    return (
      <div>
        <Helmet>
          <title>{'Das was Sie über uns wissen sollten — ' + siteTitle}</title>
          <meta
            name="description"
            content="Seit dem Jahr 2016 existiert bereits unser
                MultiKulti Markt, mit dem wir uns endlich unseren Traum von der
                Selbstständigkeit erfüllt haben und in dem wir spezielle
                Lebensmittel und vieles mehr aus dem osteuropäischen Raum anbieten."
          />
        </Helmet>

        <HeroSection>
          <HeroSectionImageContainer
            sizes={getImageSizes(
              images,
              'Russische Lebensmittel - Borsch - Rote Suppe'
            )}
            alt=""
          />
          <HeroSectionTitle>Über uns</HeroSectionTitle>
          <HeroSectionSubTitle>
            Alles was Sie über uns wissen sollten.
          </HeroSectionSubTitle>
        </HeroSection>

        <IntroSection>
          <div className="mw8 center">
            <h3 className="fw9 f2 f1-ns db lh-title mb3 mb4-l mt0 serif black i">
              Wir sind für Sie da
            </h3>

            <div className="flex flex-wrap flex-nowrap-ns">
              <span className="serif f5 f4-ns fw9 mr3 mr4-ns pt2 bt w-20-l">
                Wer wir sind
              </span>
              <p className="black w-50-l">
                Wir, das sind <i className="fw6 light-red">Irina Wienöbst</i>{' '}
                und <i className="fw6 light-red">Reinhard Wienöbst</i>, ein
                Ehepaar aus dem schönen Ort Herzlake. Seit dem Jahr 2016
                existiert bereits unser MultiKulti Markt, mit dem wir uns
                endlich unseren Traum von der Selbstständigkeit erfüllt haben
                und in dem wir spezielle Lebensmittel und vieles mehr aus dem
                osteuropäischen Raum anbieten. Sie finden uns in der Haselünner
                Straße 23, wo wir unseren kleinen aber feinen Laden im
                Tante-Emma-Stil führen.
              </p>
            </div>
          </div>
        </IntroSection>

        <ImageSection sizes={getImageSizes(images, 'dumplings')}>
          <h3 className="f4 fw7 mt0 pv2 pv3-ns bb bb--black light-red serif">
            Unser Konzept
          </h3>
          <p className="lh-copy mt3 mb0 f6">
            <span className="fw9 f2 db lh-title mb3 mb4-l serif black i">
              Qualitativ und Lecker
            </span>
            <span className="f5 db-l black">
              Zu unserem Konzept gehört eine freundliche und einladende
              Umgebung, in der man sich beim Einkaufen wohl fühlt und die Hektik
              des Alltags auch Mal ganz vergessen kann. Bei uns können Sie sich
              ganz in Ruhe umschauen und dabei auch Mal etwas entdecken, das Sie
              noch nicht kennen. Sie finden bei uns eine große Auswahl an
              russischen und polnischen Produkten, die sowohl durch ihren
              Geschmack als auch durch ihre Qualität voll und ganz überzeugen.
            </span>
          </p>
        </ImageSection>

        <div
          className="w-100 pv3 pv6-ns bg-washed-red"
          style={{
            backgroundImage: `radial-gradient(#ffdfdf 40%, transparent), url(${bgPattern})`,
            backgroundSize: 'cover',
          }}
        >
          <Intro>
            <span className="fw9 f2 db lh-title mb3 mb4-l serif black i">
              Wir kennen uns aus
            </span>
            <i className="fw6 light-red">Irina Wienöbst</i> stammt selbst aus
            der Nähe der südrussischen Stadt Sotschi und lebt seit 1994 in
            Deutschland. Daher kennt sie aus erster Hand die Unterschiede
            zwischen den russischen und den deutschen Lebensmitteln und kann Sie
            optimal beraten, wenn Sie die richtigen Produkte für ein bestimmtes
            Gericht einkaufen möchten.
          </Intro>
        </div>

        <ImageSection
          sizes={getImageSizes(images, 'brooke-lark-385507-unsplash')}
        >
          <h3 className="f4 fw7 mt0 pv2 pv3-ns bb bb--black serif">
            Unser Vorteil
          </h3>
          <p className="lh-copy mt3 mb0 f6">
            <span className="fw9 f2 db lh-title mb3 mb4-l serif i black">
              Persönlich und Gemütlich
            </span>
            <span className="f5 db-l black">
              Bei uns können Sie den großen Vorteil eines kleinen und
              gemütlichen Ladens gegenüber den großen Supermarkt-Ketten selbst
              erleben, denn wir haben immer Zeit für Sie und beraten Sie gerne
              bei Fragen, hören uns Ihre Wünsche an und lassen uns auch durchaus
              gerne von Ihrem Tag erzählen.
            </span>
          </p>
        </ImageSection>

        <PreFooter />
      </div>
    )
  }
}

export default BlogIndex

export const pageQuery = graphql`
  query AboutQuery {
    site {
      siteMetadata {
        title
      }
    }
    allImageSharp {
      edges {
        node {
          id
          sizes(maxWidth: 1920) {
            ...GatsbyImageSharpSizes
          }
        }
      }
    }
  }
`

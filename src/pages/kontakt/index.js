import React from 'react'
import get from 'lodash/get'
import Helmet from 'react-helmet'
import {getImageSizes} from '../../util/getImage'

import {
  HeroSection,
  HeroSectionTitle,
  HeroSectionSubTitle,
  HeroSectionImageContainer
} from '../../components/HeroSection'

class BlogIndex extends React.Component {
  render() {
    const siteTitle = get(this, 'props.data.site.siteMetadata.title')
    // Get the image array out of props.data
    const images = get(this, 'props.data.allImageSharp.edges')

    return (
      <div>
        <Helmet>
          <title>{'Kontakt — So können Sie uns erreichen — ' + siteTitle}</title>
          <meta
            name="description"
            content="Seit dem Jahr 2016 existiert bereits unser
                MultiKulti Markt, mit dem wir uns endlich unseren Traum von der
                Selbstständigkeit erfüllt haben und in dem wir spezielle
                Lebensmittel und vieles mehr aus dem osteuropäischen Raum anbieten."
          />
        </Helmet>
        <HeroSection>
          <HeroSectionImageContainer sizes={getImageSizes(images, "andy-chilton")} alt="" />
          <HeroSectionTitle>Kontakt</HeroSectionTitle>
          <HeroSectionSubTitle>
            So können Sie uns erreichen.
          </HeroSectionSubTitle>
        </HeroSection>

        <div className="mw9 mh3 pa3 pa5-ns mb3 mb5-ns flex center-ns flex-wrap justify-center ba light-red bg-white z-5 relative " style={{marginTop: "-20vh"}}>
          
          <div className="w-100 w-50-ns pr4-ns">
            <h3 className="f4 fw7 mt0 mb3 pb3 bb bb--black serif">
              Auf der Karte
            </h3>
            <iframe
              className="w-100"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2033.7737467581119!2d7.595933914669875!3d52.6856541068574!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6484ac78eb0361b1!2sMultiKulti+Markt+-+russischer+und+polnischer+Lebensmittelmarkt!5e0!3m2!1sen!2sde!4v1526916956667"
              frameborder="0"
              style={{ border: 0, flex: '1 0 auto', height: 500 }}
              allowfullscreen=""
            />
          </div>

          <div className="w-100 w-50-ns pl4-ns">
            <p className="mb4 lh-copy black">
              <h3 className="f4 fw7 mt0 mb3 pb3 bb bb--black serif light-red">
                Telefon
              </h3>
              <span>
                <i>Telefonisch erreichen Sie uns während unserer Öffnungszeiten unter: </i> <br/>
                <a className="link light-red mt0 fw6" href="tel:+4959628772362">
                  05962 8772362
                </a>
              </span>
            </p>

            <p className="mb4 lh-copy black">
              <h3 className="f4 fw7 mt0 mb3 pv3 bb light-red serif">
                Adresse
              </h3>
              <span>
                MultiKulti Markt <br/>
                Haselünner Str. 23 <br/>
                49770 Herzlake <br/>
              </span>
            </p>

            <p className="mb4 lh-copy black">
              <h3 className="f4 fw7 mt0 mb3 pv3 bb light-red serif">
                Öffnungszeiten
              </h3>
              <span className="mb3 db">
                Montag: <br/>
                <b className="tracked">11:00 - 19:00</b> <br/>
              </span>
              <span className="mb3 db">
                Dienstag - Samstag: <br/> 
                <b className="tracked">10:00 - 19:00</b>
              </span>
            </p>
          </div>
        </div>
      </div>
    )
  }
}

export default BlogIndex


export const pageQuery = graphql`
  query KontaktQuery {
    site {
      siteMetadata {
        title
      }
    }
    allImageSharp {
      edges {
        node {
          id
          sizes (maxWidth: 1920) {
            ...GatsbyImageSharpSizes
          }
        }
      }
    }
  }
`
import React from 'react'
import get from 'lodash/get'
import Helmet from 'react-helmet'
import { getImageSizes } from '../../util/getImage'
import Img from 'gatsby-image'

import {
  HeroSection,
  HeroSectionTitle,
  HeroSectionSubTitle,
  HeroSectionImageContainer,
} from '../../components/HeroSection'
import { IntroSection } from '../../components/Type'

import { PreFooter } from '../../components/PreFooter'

import bgPattern from '../../images/3.svg'

class BlogIndex extends React.Component {
  render() {
    const siteTitle = get(this, 'props.data.site.siteMetadata.title')
    // Get the image array out of props.data
    const images = get(this, 'props.data.allImageSharp.edges')

    return (
      <div>
        <Helmet>
          <title>
            {'Spezialitäten — Das macht uns besonders — ' + siteTitle}
          </title>
          <meta
            name="description"
            content="Zu den in unserem MultiKulti Markt angebotenen Produkten gehören auch einige köstliche Spezialitäten, die Ihnen sicherlich gefallen werden. Dazu gehört beispielsweise unser hochwertiger russischer Kaviar, der bei keiner großen Feier fehlen sollte."
          />
        </Helmet>

        <HeroSection>
          <HeroSectionImageContainer
            sizes={getImageSizes(images, 'russian-pancake-and-caviar')}
            alt="Pfannkuchen mit rotem Kaviar"
          />
          <HeroSectionTitle>Spezialitäten</HeroSectionTitle>
          <HeroSectionSubTitle>Das macht uns besonders.</HeroSectionSubTitle>
        </HeroSection>

        <IntroSection className="mw9">
          <h3 className="f4 fw7 mt0 pv3 bb bb--black light-red serif">
            Außergewöhnlich
          </h3>
          <span className="fw9 f2 f1-ns db lh-title mb3 mb4-l serif black i">
            Unsere besonderen Spezialitäten
          </span>

          <div className="flex flex-wrap">
            <span className="serif f4 fw9 mv3 mv0-ns pt2 bt w-10-ns">
              Kaviar
            </span>
            <p className="black ph4-ns w-40-ns">
              Zu den in unserem MultiKulti Markt angebotenen Produkten gehören
              auch einige köstliche Spezialitäten, die Ihnen sicherlich gefallen
              werden. Dazu gehört beispielsweise unser hochwertiger russischer
              Kaviar, der bei keiner großen Feier fehlen sollte.
            </p>

            <div className="w-50-ns h5 h-100-ns img-container">
              <Img sizes={getImageSizes(images, 'kaviar-2')} alt="" />
            </div>
          </div>
        </IntroSection>

        <section
          className="flex flex-wrap w-100 center pa3 pa6-ns bg-light-yellow"
          style={{
            backgroundImage: `radial-gradient(#fbf1a9 40%, transparent), url(${bgPattern})`,
            backgroundSize: 'cover',
          }}
        >
          <div className="flex flex-wrap mw8 center lh-copy">
            <span className="serif f4 fw9 mv3 mv0-ns pt2 bt w-10-ns light-red">
              Gewürze
            </span>

            <p className="black ph4-ns w-40-ns">
              Zu unseren weiteren Spezialitäten gehören auch unsere Gewürze. Mit
              diesen Gewürzen können Sie Ihren Gerichten den typisch russischen
              oder polnischen Geschmack verleihen.
            </p>

            <div className="w-50-ns h5 h-100-ns img-container">
              <Img
                sizes={getImageSizes(
                  images,
                  'large_square_Star_Anise__Ground__close'
                )}
                alt=""
              />
            </div>
          </div>

          <div className="flex flex-wrap mw8 center lh-copy">
            <span className="serif f4 fw9 mv3 mv0-ns pt2 bt w-10-ns light-red">
              Süßigkeiten
            </span>

            <p className="black ph4-ns w-40-ns">
              Ansonsten finden Sie bei uns sehr viele typisch russische süße
              Leckereien, wie Gebäck, Halva, Schokolade, Pralinen oder Bonbons.
            </p>

            <div className="w-50-ns h5 h-100-ns img-container">
              <Img
                sizes={getImageSizes(images, 'chocolate.png')}
                alt="Russische und polnische Schokolade"
              />
            </div>
          </div>
        </section>

        <PreFooter />
      </div>
    )
  }
}

export default BlogIndex

export const pageQuery = graphql`
  query SpecialQuery {
    site {
      siteMetadata {
        title
      }
    }
    allImageSharp {
      edges {
        node {
          id
          sizes(maxWidth: 1920) {
            ...GatsbyImageSharpSizes
          }
        }
      }
    }
  }
`

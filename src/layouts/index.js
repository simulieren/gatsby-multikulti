import React from 'react'

import "tachyons";
import "./index.css";

import {Header} from '../components/Header'
import {Footer} from '../components/Footer'

class Template extends React.Component {
  render() {
    const { location, children } = this.props
    
    return (
      <div>
        <Header />
        {children()}
        <Footer />
      </div>
    )
  }
}

export default Template

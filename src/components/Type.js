import React from 'react'
import styled from "styled-components"
import Link from "gatsby-link"

export const Intro = styled.section.attrs({
  className: "intro mv3 mv6-ns mw6 mh3 center-ns f5 f4-ns lh-copy relative z-5",
})``

export const IntroSectionWrapper = styled.section.attrs({
  className: "intro mv3 mv6-ns mw9 center-ns mh3 f5 f4-ns lh-copy z-5 relative pa1 pa5-ns ba light-red"
})`
  margin-top: -20vh;
`

export class IntroSection extends React.Component {
  render () {
    return (
      <IntroSectionWrapper>
        <div className="pa3 pa5-ns ba light-red bg-white">
          {this.props.children}
        </div>
      </IntroSectionWrapper>
    )
  }
}

export const StyledLink = styled(Link)`
  text-decoration: none;
  box-shadow: none;
`

export const FooterLink = styled(Link).attrs({
  className: "f6 ttu tracked dib w-100 w-auto-ns mb3 mh4-ns link white dim"
})`
  text-decoration: none;
  box-shadow: none;
`

export const NavLink = styled(Link).attrs({
  className: "f7 f6-ns link bg-animate color-animate white hover-bg-white hover-black dib pa2 pa3-ns"
})`
  text-decoration: none;
  box-shadow: none;

  &.active {
    background: white;
    color: black;
  }
`

// Buttons

const defaultButtonClasses = "fw9 ph2 dib relative " 
const defaultButtonStyling = `
  transition: all .25s ease;
  text-decoration: none;
  border-bottom: none;

  &::after {
    transition: all .25s ease;
    z-index: -1;
    content: "";
    display: block;
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    transform: scaleY(.05);
    transform-origin: bottom;
    background-color: #ff725c;
  }

  &:hover {
    color: #fff;
  }

  &:hover::after {
    transform: scaleY(1);
  }
`

export const SecondaryButtonSmall = styled(Link).attrs({
  className: defaultButtonClasses + "f6 pv2 mb2"
})`${defaultButtonStyling}`

export const SecondaryButtonMedium = styled(Link).attrs({
  className: defaultButtonClasses + "f5 pv3 mb2"
})`${defaultButtonStyling}`

export const SecondaryButtonLarge = styled(Link).attrs({
  className: defaultButtonClasses + "f4 pv3 mb2"
})`${defaultButtonStyling}`
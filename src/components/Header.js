import React from 'react'
import Link from 'gatsby-link'
import Helmet from 'react-helmet'

import { StyledLink, NavLink } from './Type'

import { Logo } from './Logo'

export class Header extends React.Component {
  render() {
    return (
      <header className="white tc pv4-ns absolute t-0 flex flex-column justify-center items-center w-100 z-999">
        <StyledLink to="" className="pa1 link no-underline pb0">
          <Logo/>
        </StyledLink>
        <span className="mt0 mb3-l f5 f4-ns fw5">Markt</span>
        <h2 className="dn db-ns mt2 mt3-l mb0 f7 fw4 ttu tracked">
          Russische und polnische Lebensmittel
        </h2>
        <nav className="bt bb b--white tc mw7 center mt3 mt4-ns ttu tracked">
          {/* <NavLink exact activeClassName="active" to="/">Startseite</NavLink> */}
          <NavLink activeClassName="active" to="/produkte/">Produkte</NavLink>
          <NavLink activeClassName="active" to="/spezialitaeten/">Spezialitäten</NavLink>
          <NavLink activeClassName="active" to="/ueber-uns/">Über uns</NavLink>
          <NavLink activeClassName="active" to="/kontakt/">Kontakt</NavLink>
        </nav>
      </header>
    )
  }
}

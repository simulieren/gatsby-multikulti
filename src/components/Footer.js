import React from 'react'
import Link from 'gatsby-link'

import {StyledLink, FooterLink} from "./Type"

import { Logo } from './Logo'

export class Footer extends React.Component {

  render() {
    return (
      <footer className="pv3 pv4-ns ph3 ph5-m ph6-l bg-light-red">
        <span className="tc serif f4 white db mv3 mv4-ns">Navigation</span>
        <div className="tc pb3 pb4-ns mb3 mb4-ns bb b--white">
          <FooterLink to="/" title="Startseite">Startseite</FooterLink>
          <FooterLink to="/produkte/" title="Produkte">Produkte</FooterLink>
          <FooterLink to="/spezialitaeten/"  title="Spezialitäten">Spezialitäten</FooterLink>
          <FooterLink to="/ueber-uns/"  title="Über uns">Über uns</FooterLink>
          <FooterLink to="/kontakt/"  title="Kontakt">Kontakt</FooterLink>
        </div>

        <div className="flex flex-column w-100 items-center">
          <Logo/>
          <span className="mt1 mb3 f5 f4-ns fw5 white">Markt</span>
        </div>

        <div className="lh-copy">
          <small className="f6 db tc white mb2"><b>MultiKulti Markt</b>, Haselünner Str. 23, 49770 Herzlake</small>
          <small className="f6 db tc white mb4">Telefon: <a className="link white dim" href="tel:+4959628772362">05962 8772362</a></small>
          <small className="f6 db tc white mb2">Öffnungszeiten:</small>
          <small className="f6 db tc white mb2">Montag: <br/><b className="tracked">11:00 - 19:00</b></small>
          <small className="f6 db tc white">Dienstag bis Samstag: <br/><b className="tracked">10:00 - 19:00</b></small>
        </div>
        <div className="tc mt4">
          <StyledLink to="/impressum/"    title="Impressum" className="f7 ttu tracked dib mh2 link white dim">Impressum</StyledLink>
          <StyledLink to="/datenschutz/"  title="Datenschutz" className="f7 ttu tracked dib mh2 link white dim">Datenschutz</StyledLink>
        </div>
      </footer>
    )
  }
}



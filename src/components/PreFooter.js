import React from 'react'
import styled from "styled-components"

import { Intro, SecondaryButtonLarge } from './Type'

import bgPattern from '../images/2.svg'

const PreFooterSection = styled.section.attrs({
  className: "w-100 bg-washed-red pv2  pv4-ns"
})`
  background-image: radial-gradient(#ffdfdf 40%, transparent), url(${bgPattern});
  background-size: cover;
  background-position: center;
  // background-attachment: fixed;
`

export const PreFooter = props => (
  <PreFooterSection>
    <Intro className="z-2">
      <h3 className="fw9 f2 db lh-title mt0 mb3 mb4-l serif black i">
        Kommen Sie uns besuchen!
      </h3>
      Wenn Sie also nach einem Laden für russische und polnische Spezialitäten
      suchen und dabei Wert auf eine persönliche Note legen, dann sind Sie in
      unserem kleinen Familienunternehmen genau an der richtigen Adresse!
      <div className="mv3 mv4-ns flex justify-center">
        <SecondaryButtonLarge to="/kontakt/" className="light-red">
          So finden Sie uns!
        </SecondaryButtonLarge>
      </div>
    </Intro>
  </PreFooterSection>
)

import React from 'react'
import styled from "styled-components"
import Img from 'gatsby-image'

export const HeroSection = styled.section.attrs({
  className: "center sans-serif flex flex-column justify-center items-center vh-100 bg-light-red relative overflow-hidden",
})`
&:after {
  pointer-events: none;
  content: '';
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 150%;
  background: radial-gradient(rgba(255,255,255, 0) 40%, rgba(255,255,255, 1) 80%);
  z-index: 2;
}

&:before {
  pointer-events: none;
  content: '';
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 40%;
  background: linear-gradient(rgba(255,255,255, 0) 0%, rgba(255,255,255, 1) 100%);
  z-index: 2;
}

.gatsby-image-outer-wrapper {
  width: 100%;
  opacity: .7;
  height: 100%;
}

.gatsby-image-wrapper {
  height: 100%;
}
`

export const HeroSectionTitle = styled.h1.attrs({
  className: "relative f2 f-6-ns fw5 z-5 white mb2 mb4-ns mh2"
})`
  text-shadow: 0 1px 3px rgba(0,0,0,0.5);
`

export const HeroSectionSubTitle = styled.h2.attrs({
  className: "relative f3 f2-ns fw9 i z-5 white pt3 mh2 pt4-ns mt0 bt b--white serif tc"
})`
  text-shadow: 0 1px 3px rgba(0,0,0,0.5);
`

// export const HeroSectionImage = styled(Img).attrs({
//   className: "min-h-100 h-100 h-auto-ns min-h-100-ns mw-none mw-100-ns o-80 absolute db z-1"
// })`
//   min-width: 100%;
// `

export class HeroSectionImageContainer extends React.Component {
  render () {
    console.log("HERO SECTION IMAGE CONTAINER", this.props);
    
    return (
      <div className="absolute absolute--fill overflow-hidden flex justify-center items-center" style={{objectFit: "cover"}}>
        <Img {...this.props} />
      </div>
    )
  }
}


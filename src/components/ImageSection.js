import React from 'react'
import styled from 'styled-components'
import Img from 'gatsby-image'

const ImageSectionContainer = styled.section.attrs({
  className: "flex pv3 pv7-ns center relative w-100 justify-center items-center overflow-hidden"
})`
.gatsby-image-outer-wrapper {
  width: 100%;
  height: 100%;
  min-height: 100%;
}
`

export const ImageSection = props => (
  <ImageSectionContainer>
    <div className="absolute absolute--fill">
      <Img className="w-100 min-h-100 absolute dn db-ns" sizes={props.sizes} alt={props.alt} title={props.title} />
    </div>
    <div className="mw7 z-2">
      <div className="w-100 pa3 pa5-ns mb0 bg-white">
        <div className="pa3 pa5-ns outline light-red">{props.children}</div>
      </div>
    </div>
  </ImageSectionContainer>
)

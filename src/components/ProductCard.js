import React from 'react'
import styled from "styled-components"
import Img from 'gatsby-image'

const ProductCardContainer = styled.article.attrs({
  className: "w-100 w-50-m w-33-l ph3 mb4 mb5-ns"
})`
.gatsby-image-outer-wrapper {
  width: 100%;
  height: 50vh;
}
`

export const ProductCard = props => (
<ProductCardContainer>
  <div className="vh-50 relative">
    <div className="absolute absolute--fill overflow-hidden flex justify-center items-center" style={{objectFit: "cover"}}>
      <Img style={{minWidth: "100%", minHeight: "100%"}} sizes={props.sizes} alt={props.alt} title={props.title} />
    </div>
    <div className="absolute absolute--fill ma3 ma4-ns ba white"></div>
  </div>
  <span className="db serif f4 f3-ns fw9 lh-title mt3 mt4-ns pt2 pt3-ns bt light-red">
    {props.title}
  </span>
</ProductCardContainer>
)
